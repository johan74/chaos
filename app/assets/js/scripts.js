///  image reveal
var images = document.querySelectorAll('.image');

if (images != null) {
  images.forEach(image => {
    gsap.from(image, {
      scrollTrigger: {
        trigger:image,
        start:"top 50%",
        toggleClass:{targets:image, className:"image--reveal"},
        toggleActions:"restart pause resume none"  
      },
      duration:1,
      ease:Strong.easeOut
    })
  });
}

// nav reveal 
var nav = document.querySelector('.nav');

if (nav != null) {
  gsap.to(nav, {
    ease: "ease",
    scrollTrigger: {
      trigger: nav,
      endTrigger: "nothing",
      start: "top top",
      end: "bottom bottom",
      scrub: true,
      pin: true,
      pinSpacing:false,
      toggleClass:{targets:nav, className:"nav--reveal"}

    }
  });
}

///  shard reveal
var shards = document.querySelectorAll('.shards');

if (shards != null) {
  shards.forEach(shard => {
    gsap.from(shard, {
      scrollTrigger: {
        trigger:shard,
        start:"top 50%",
        toggleClass:{targets:shard, className:"shards--reveal"},
        toggleActions:"restart pause resume none"  
      },
      duration:1,
      ease:Strong.easeOut
    })
  });
}







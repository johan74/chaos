const gulp = require("gulp");
const { parallel, series } = require("gulp");
const uglify = require("gulp-uglify");
// const sass = require("gulp-sass");
var sass = require('gulp-dart-sass');
const concat = require("gulp-concat");
const browserSync = require("browser-sync").create(); //https://browsersync.io/docs/gulp#page-top
const autoprefixer = require('gulp-autoprefixer');
const babel = require('gulp-babel');
const del = require('del');







// Compile Main Sass
function mainCss(cb) {
    gulp.src("src/scss/**/*.scss")
        .pipe(sass({ outputStyle: "compressed" }).on("error", sass.logError))
        .pipe(autoprefixer({
            browserlist: ['last 6 versions'],
            cascade: false
        }))
        .pipe(gulp.dest("app/assets/css/"))
        // Stream changes to all browsers
        .pipe(browserSync.stream());
    cb();
}


// Watch Files
function watch_files() {
    browserSync.init({
        server: "app"
    });
    gulp.watch("src/scss/**/*.scss", mainCss).on("change", browserSync.reload);
    gulp.watch("app/assets/js/*.js").on("change", browserSync.reload);
    gulp.watch("app/**/*.html").on("change", browserSync.reload);
}

// Default 'gulp' command with start local server and watch files for changes.
exports.default = series(mainCss, watch_files);



